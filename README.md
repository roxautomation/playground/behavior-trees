

# py_trees playground

Playing with behavior trees

## Documentation

* [py_trees docs](https://py-trees.readthedocs.io/en/devel/index.html)
* [py_trees ROS tutorial](https://py-trees-ros-tutorials.readthedocs.io/en/release-2.0.x/index.html)


## Quick start

1. start devcontainer. It comes pre-installed with `py_trees` packages.
2. work with files in `btrees` folder.


## What goes where

* `btrees` Contains proto code. It is *not* a python package. Needs to be added to `PYTHONPATH`.
* `docker` folder contains dockerfiles for building base system and app images.
* `.gitlab-ci.yml` takes care of the building steps.


**Note:** system image is built manually to avoid overcomplicated ci rules.

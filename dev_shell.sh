#!/bin/bash
set -e

IMG_NAME="local/btrees"

# build docker image
docker build -t $IMG_NAME -f ./docker/Dockerfile  ./docker

# run docker image
docker run --rm -it \
           --network=host \
           -v $(pwd):/workspaces/btrees \
            $IMG_NAME bash

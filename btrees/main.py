#!/usr/bin/env python3
"""
  Main entry point for the application.
"""
import asyncio


async def main():
    """main coroutine"""

    while True:
        print(".", end="", flush=True)
        await asyncio.sleep(1)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("Done.")
